﻿namespace Slack_Recorder
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.saveDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.recordActivatedCheckBox = new System.Windows.Forms.CheckBox();
            this.runAtStartUpCheckBox = new System.Windows.Forms.CheckBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.openButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.helpButton = new System.Windows.Forms.Button();
            this.saveDirectoryLabel = new System.Windows.Forms.Label();
            this.openGridButton = new System.Windows.Forms.Button();
            this.openSaveDirectoryButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Slack Recoder";
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // saveDirectoryTextBox
            // 
            this.saveDirectoryTextBox.Enabled = false;
            this.saveDirectoryTextBox.Location = new System.Drawing.Point(12, 42);
            this.saveDirectoryTextBox.Multiline = true;
            this.saveDirectoryTextBox.Name = "saveDirectoryTextBox";
            this.saveDirectoryTextBox.Size = new System.Drawing.Size(181, 23);
            this.saveDirectoryTextBox.TabIndex = 0;
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(199, 42);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(73, 23);
            this.browseButton.TabIndex = 1;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // recordActivatedCheckBox
            // 
            this.recordActivatedCheckBox.AutoSize = true;
            this.recordActivatedCheckBox.Checked = true;
            this.recordActivatedCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.recordActivatedCheckBox.Location = new System.Drawing.Point(12, 134);
            this.recordActivatedCheckBox.Name = "recordActivatedCheckBox";
            this.recordActivatedCheckBox.Size = new System.Drawing.Size(75, 17);
            this.recordActivatedCheckBox.TabIndex = 2;
            this.recordActivatedCheckBox.Text = "Recording";
            this.recordActivatedCheckBox.UseVisualStyleBackColor = true;
            this.recordActivatedCheckBox.CheckedChanged += new System.EventHandler(this.recordActivatedCheckBox_CheckedChanged);
            // 
            // runAtStartUpCheckBox
            // 
            this.runAtStartUpCheckBox.AutoSize = true;
            this.runAtStartUpCheckBox.Location = new System.Drawing.Point(100, 134);
            this.runAtStartUpCheckBox.Name = "runAtStartUpCheckBox";
            this.runAtStartUpCheckBox.Size = new System.Drawing.Size(93, 17);
            this.runAtStartUpCheckBox.TabIndex = 9;
            this.runAtStartUpCheckBox.Text = "Run at startup";
            this.runAtStartUpCheckBox.UseVisualStyleBackColor = true;
            this.runAtStartUpCheckBox.CheckedChanged += new System.EventHandler(this.runAtStartUpCheckBox_CheckedChanged);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(649, 127);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(123, 23);
            this.deleteButton.TabIndex = 15;
            this.deleteButton.Text = "Delete selected record";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(302, 127);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(132, 23);
            this.openButton.TabIndex = 14;
            this.openButton.Text = "Open selected record";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(302, 10);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(470, 111);
            this.dataGridView1.TabIndex = 16;
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Location = new System.Drawing.Point(484, 127);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(123, 23);
            this.btn_Refresh.TabIndex = 15;
            this.btn_Refresh.Text = "Refresh Table";
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // helpButton
            // 
            this.helpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.helpButton.Image = ((System.Drawing.Image)(resources.GetObject("helpButton.Image")));
            this.helpButton.Location = new System.Drawing.Point(251, 10);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(21, 23);
            this.helpButton.TabIndex = 17;
            this.helpButton.UseVisualStyleBackColor = true;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // saveDirectoryLabel
            // 
            this.saveDirectoryLabel.AutoSize = true;
            this.saveDirectoryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.saveDirectoryLabel.Location = new System.Drawing.Point(12, 13);
            this.saveDirectoryLabel.Name = "saveDirectoryLabel";
            this.saveDirectoryLabel.Size = new System.Drawing.Size(80, 17);
            this.saveDirectoryLabel.TabIndex = 18;
            this.saveDirectoryLabel.Text = "Save folder";
            // 
            // openGridButton
            // 
            this.openGridButton.Image = ((System.Drawing.Image)(resources.GetObject("openGridButton.Image")));
            this.openGridButton.Location = new System.Drawing.Point(199, 80);
            this.openGridButton.Name = "openGridButton";
            this.openGridButton.Size = new System.Drawing.Size(75, 73);
            this.openGridButton.TabIndex = 19;
            this.openGridButton.UseVisualStyleBackColor = true;
            this.openGridButton.Click += new System.EventHandler(this.openRecordsButton_Click);
            // 
            // openSaveDirectoryButton
            // 
            this.openSaveDirectoryButton.Location = new System.Drawing.Point(12, 80);
            this.openSaveDirectoryButton.Name = "openSaveDirectoryButton";
            this.openSaveDirectoryButton.Size = new System.Drawing.Size(181, 30);
            this.openSaveDirectoryButton.TabIndex = 20;
            this.openSaveDirectoryButton.Text = "Open save folder";
            this.openSaveDirectoryButton.UseVisualStyleBackColor = true;
            this.openSaveDirectoryButton.Click += new System.EventHandler(this.goToSaveFolderButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 163);
            this.Controls.Add(this.openSaveDirectoryButton);
            this.Controls.Add(this.openGridButton);
            this.Controls.Add(this.saveDirectoryLabel);
            this.Controls.Add(this.helpButton);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.runAtStartUpCheckBox);
            this.Controls.Add(this.recordActivatedCheckBox);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.saveDirectoryTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(800, 202);
            this.MinimumSize = new System.Drawing.Size(300, 202);
            this.Name = "Form1";
            this.Text = "Slack Recoder";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.TextBox saveDirectoryTextBox;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.CheckBox recordActivatedCheckBox;
        private System.Windows.Forms.CheckBox runAtStartUpCheckBox;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Refresh;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.Label saveDirectoryLabel;
        private System.Windows.Forms.Button openGridButton;
        private System.Windows.Forms.Button openSaveDirectoryButton;
    }
}

